# Development Plugins

Integrated Genome Browser (IGB) runs in an OSGi container, which supports adding and removing pluggable Apps while IGB is running. 
Development plugins are such pluggable Apps. 

####To build from the command line:

1. Clone the [development plugins repository](https://bitbucket.org/pkulka10/developmentplugins) to a preferred location on your machine.  

2. Inside the local copy, check out the branch you wish to build. You can make changes to one or more plugins as required.

For Example:

```
git clone https://bitbucket.org/pkulka10/developmentplugins
cd developmentplugins
```

To build development plugins using maven use following command.

```
mvn clean install
```

OR

To build a specific plugin use following maven command.

```
mvn -f <plugin_folder_name> -P bitbucket-pipeline-profile
```

OR

To build a consolidated OBR index file for all plugins use following maven command.
    
```
mvn clean install -P bitbucket-pipeline-profile -DobrRepository=<project_build_directory_path>
```

**Note**: Instructions above assume you have Java installed on your computer.


####To setup bitbucket pipeline:

1. Create a bitbucket configuration file (.yml) in your project's base directory.

2. To build the project specify docker image(i.e. build environment) as a value of **name** key under **image** key in the configuration file.

3. List down the steps required to build the project as a value of **step** key under **pipelines** > **default** key.

4. Go to the **Settings** section of the repository and click on **pipelines** > **Settings**.

5. Click on enable pipelines.

6. Go to bitbucket **Settings** section and create new **App Password**.

7. Go to repository **Settings** section and click in **Repository Variables** under **Pipelines**.

8. Create one repository variable named **BB_AUTH_STRING** and paste <bitbucket_username>:<App_password> as a value of the variable.

**Note**: Default configuration is triggered whenever any change is pushed to the master. If you want to configure automatic build with pipelines 
then you can write a custom pipeline configuration for the same.
 

####To build from the bitbucket pipeline:

Whenever a change is pushed to the master branch of development plugins repository, a pipeline build is triggered automatically.

1. Go To **Branches** section of the repository.

2. Click **"..."** on the master branch and select **Run pipeline for a branch**.

3. If you want to build a consolidated OBR index file for all plugins then  select **BuildAllPlugings** option and click on **Run**.
   OR
   If you want to build a specific plugin then select plugin name from the dropdown and click on **Run**.
   

####To run the plugins:

1. If you want to run single app, then build the plugin using custom pipeline configuration steps mentioned above.

2. Go to **Downloads** section of the repository. Here the OBR index file is be generated for the plugin once the pipeline run is successful. 
Copy the url of this webpage.

3. Start IGB. Open **Manage Repositories** from **Plugins Manager**. 

4. Click on **Add Repository**. 

5. Write a name for new repository and paste the url.

6. Then click on **Save Changes**. Click on the Plugin which is there in the left column and **Install** it.

**Note**: If you have chosen **BuildAllPlugins** custom pipeline configuration then a consoldated OBR index file will be generated in the 
**Downloads** section of the repository. Follow same steps which are mentioned above. Instead of a single plugin, a list of plugins will be shown 
when you will add this url as a new repository in IGB. You can choose required plugin for testing.



#### To contribute

Use fork-and-branch workflow:

1. Fork and clone the [development plugins repository](https://bitbucket.org/pkulka10/developmentplugins) to a preferred location on your machine.  
2. Create branches specific to the changes you want to make, push to your fork.
3. Issue pull requests to the team repository's master branch from the branch on your fork.
